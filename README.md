# OpenML dataset: sulfur

https://www.openml.org/d/44003

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "classification on numerical features" benchmark. Original description: 
 
&quot;The sulfur recovery unit (SRU) removes environmental pollutants from acid gas
streams before they are released into the atmosphere. Furthermore, elemental sulfur
is recovered as a valuable by-product.&quot;

5 inputs variables are gas and air flows.
2 outputs to predict are H2S and SO2 concentrations

See Appendix A.5 of Fortuna, L., Graziani, S., Rizzo, A., Xibilia, M.G. &quot;Soft Sensors for Monitoring and Control of Industrial Processes&quot; (Springer 2007) for more info.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44003) of an [OpenML dataset](https://www.openml.org/d/44003). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44003/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44003/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44003/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

